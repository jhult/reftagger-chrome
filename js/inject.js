function insertRefagger(settings) {
	var script = document.getElementsByTagName("script")[0];
	
	var refSettings = document.createElement("script");
	refSettings.textContent = [settings];
	script.parentNode.insertBefore(refSettings, script);
	
	var refJS = document.createElement("script");
	refJS.src = chrome.extension.getURL("/js/reftagger.js");
	script.parentNode.insertBefore(refJS, script);
}

// runs sometime during page load
chrome.storage.local.get(
	{
		// defaults
    	style: 'light',
    	version: 'ESV'
	}, 
	function(items) {
		var style = items.style;
    	var version = items.version;
   		var settings = 'var refTagger = {settings: {tagChapters: true, bibleVersion: "' + version + '", tooltipStyle:"' + style + '"}};'   
		insertRefagger(settings);
	}
);

// on change of options, reload page
chrome.storage.onChanged.addListener(
    function() {
        window.location.reload();
    }
);