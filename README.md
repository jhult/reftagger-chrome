# reftagger-chrome

> A free, essential tool for Christian web users that saves you time checking sources and helps you gain context for what you're reading without ever leaving the page you're on.

Reftagger for Google Chrome is a simple tool that automatically detects Scripture references anywhere on the web and converts them into links to an online Bible. A tooltip displays the verse�s text, so your readers don�t have to leave your site to look it up.  The hover-preview also shows social icons, making it easier than ever for you to share Scripture.

## Video Overview
[![YouTube video](https://bytebucket.org/jhult/reftagger-chrome/raw/master/images/screenshot_small.png)](https://www.youtube.com/watch?v=d6Jz7kQKqcI)

## Options
![alt text](https://bytebucket.org/jhult/reftagger-chrome/raw/master/images/screenshot_options.png "Options")

## Contributions
1. This extension uses [Reftagger](https://reftagger.com) from [Faithlife](https://faithlife.com/) / [Logos Bible Software](https://www.logos.com).
2. This extension was originally developed by [Samuel Taylor](https://www.samueltaylor.org).

## Install

### Chrome Store
1. Navigate [here](https://chrome.google.com/webstore/detail/bbjhfljjadeplkjkicakncilmmlhlokf)
2. Click `ADD TO CHROME`

## Development
1.  Visit `chrome://extensions/` in Chrome
2.  Enable the **Developer mode**
3.  Click on **Load unpacked extension**     
4.  Select the cloned folder
