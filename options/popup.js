document.getElementById("styleForm").addEventListener('click', save_options);
var versionSelect = document.getElementById("versionSelect");
versionSelect.addEventListener('change', function onClick(e) {
    save_options();
});

document.addEventListener("DOMContentLoaded", init_options);

function save_options() {
    var style;
    if (document.getElementById('dark').checked) {
        style = 'dark';
    } else {
        style = 'light';
    }
    var version = document.getElementById("versionSelect").value;

    chrome.storage.local.set(
		{
            style: style,
            version: version
        },
        function() {});
}

function init_options() {
    chrome.storage.local.get(
		{
        	style: 'light',
        	version: 'ESV'
    	},
		function(items) {
			for (var i = 0; i < styleForm.length; i++) {
				if (styleForm[i].name == "backgroundStyle" && styleForm[i].value == items.style) {
					styleForm[i].checked = true;
				} else
					styleForm[i].checked = false;
			}
        var versionSelect = document.getElementById("versionSelect");
        versionSelect.value = items.version;
    });
}